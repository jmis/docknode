"use strict";

var browserify = require("browserify");
var gulp = require("gulp");
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var mocha = require("gulp-mocha");
var uglify = require("gulp-uglify");
var transform = require("vinyl-transform");

gulp.task("run-tests", function () {
  return gulp.src("./test/**/*.js", { read: false })
    .pipe(mocha({ reporter: "spec" }));
});

gulp.task("build", function () {
  return browserify({ entries: ["./src/index.js"], debug: false })
    .require("./src/index.js", { expose: "docking-model"})
    .bundle()
    .pipe(source('docking-model.js'))
    .pipe(buffer())
    .pipe(gulp.dest("build"));
});
