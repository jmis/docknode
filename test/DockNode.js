var test = require("unit.js");
var DockNode = require("../src/DockNode.js");
var Enums = require("../src/DockingEnums.js");

describe("DockNode", function()
{
	describe("addInside", function()
	{
		it("then the new node should consume all of root node's width", function()
		{
			var rootNode = new DockNode(100, 0);
			var northNode = new DockNode(0, 0);
			rootNode.addInside(Enums.nodePosition.north, northNode);
			test.value(northNode.totalDimensionSize(Enums.dimensions.width)).isEqualTo(rootNode.totalDimensionSize(Enums.dimensions.width));
		});

		it("then the new node should not overlap existing node nodes", function()
		{
			var rootNode = new DockNode(100, 100);
			var westNode = new DockNode(0, 0)
			var northNode = new DockNode(0, 0);

			rootNode.addInside(Enums.nodePosition.west, westNode);
			rootNode.addInside(Enums.nodePosition.north, northNode);
			test.value(rootNode.totalDimensionSize(Enums.dimensions.width)).isEqualTo(100);
			test.value(northNode.totalDimensionSize(Enums.dimensions.width)).isEqualTo(75);
		});

		it("then new node should not overlap the west and east nodes", function()
		{
			var rootNode = new DockNode(100, 100);
			var westNode = new DockNode(0, 0)
			var northNode = new DockNode(0, 0);
			var eastNode = new DockNode(0, 0);
			rootNode.addInside(Enums.nodePosition.west, westNode);
			rootNode.addInside(Enums.nodePosition.east, eastNode);
			rootNode.addInside(Enums.nodePosition.north, northNode);
			test.value(northNode.totalDimensionSize(Enums.dimensions.width)).isEqualTo(50);
		});

		it("then it consumes height from the center", function()
		{
			var rootNode = new DockNode(100, 100);
			var northNode = new DockNode(0, 0);
			rootNode.addInside(Enums.nodePosition.north, northNode);
			test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
			test.value(northNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(20);
			test.value(rootNode.childNodeDimensionSize(Enums.nodePosition.center, Enums.dimensions.height)).isEqualTo(75);
		});
	});

	describe("when the height of", function()
	{
		describe("the north node", function()
		{
			describe("increases", function()
			{
				it("then it takes height from the center", function()
				{
					var rootNode = new DockNode(100, 100);
					var northNode = new DockNode(0, 0);
					rootNode.addInside(Enums.nodePosition.north, northNode);
					rootNode.resize(Enums.nodePosition.north, 50);
					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
					test.value(rootNode.childNodeDimensionSize(Enums.nodePosition.center, Enums.dimensions.height)).isEqualTo(45);
					test.value(northNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(50);
				});

				it("then it removes height from clipped east and west nodes", function()
				{
					var rootNode = new DockNode(100, 100);
					var northNode = new DockNode(0, 0);
					var eastNode = new DockNode(0, 0);
					rootNode.addInside(Enums.nodePosition.north, northNode);
					rootNode.addInside(Enums.nodePosition.east, eastNode);

					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
					test.value(eastNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(75);

					rootNode.resize(Enums.nodePosition.north, 60);

					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
					test.value(rootNode.childNodeDimensionSize(Enums.nodePosition.center, Enums.dimensions.height)).isEqualTo(35);
					test.value(northNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(60);
					test.value(eastNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(35);
				});
			});

			describe("decreases", function()
			{
				it("then it adds height to the center", function()
				{
					var rootNode = new DockNode(100, 100);
					var northNode = new DockNode(0, 0);
					rootNode.addInside(Enums.nodePosition.north, northNode);
					rootNode.resize(Enums.nodePosition.north, 10);

					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
					test.value(rootNode.childNodeDimensionSize(Enums.nodePosition.center, Enums.dimensions.height)).isEqualTo(85);
					test.value(northNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(10);
				});
			});
		});
	});

	describe("when scaling", function()
	{
		describe("the height", function()
		{
			describe("of a node with no docked nodes", function()
			{
				it("then its height can be increased from 0", function()
				{
					var rootNode = new DockNode(0, 0);
					rootNode.scaleTo(Enums.dimensions.height, 200);
					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(200);
				});

				it("then its height can be decreased", function()
				{
					var rootNode = new DockNode(0, 0);
					rootNode.scaleTo(Enums.dimensions.height, 10);
					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(10);
				});

				it("then its height can be scaled multiple times", function()
				{
					var rootNode = new DockNode(0, 0);
					rootNode.scaleTo(Enums.dimensions.height, 10);
					rootNode.scaleTo(Enums.dimensions.height, 100);
					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
					rootNode.scaleTo(Enums.dimensions.height, 150);
					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(150);
					rootNode.scaleTo(Enums.dimensions.height, 60);
					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(60);
				});
			})

			describe("of a node with one layer of docked nodes", function()
			{
				it("then west docked nodes should stay proportional", function()
				{
					var rootNode = new DockNode(0, 100);
					var westNode = new DockNode(0, 0);
					rootNode.addInside(Enums.nodePosition.west, westNode);
					rootNode.scaleTo(Enums.dimensions.height, 200);

					test.value(rootNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(200);
					test.value(westNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(200);
				});
			});
		})
	});

	describe("coordinates", function()
	{
		it("the root node will always be at (0 ,0)", function()
		{
			var rootNode = new DockNode(100, 100);
			var coords = rootNode.coordinates();
			test.value(coords.x).isEqualTo(0);
			test.value(coords.y).isEqualTo(0);
		});

		it("west not clipped", function()
		{
			var rootNode = new DockNode(100, 100);
			var northNode  = new DockNode(0, 0);
			var westNode = new DockNode(0, 0);
			rootNode.addInside(Enums.nodePosition.west, westNode);
			rootNode.addInside(Enums.nodePosition.north, northNode);

			var coords = westNode.coordinates();
			test.value(coords.x).isEqualTo(0);
			test.value(coords.y).isEqualTo(0);
		});

		it("west clipped by north", function()
		{
			var rootNode = new DockNode(100, 100);
			var northNode  = new DockNode(0, 0);
			var westNode = new DockNode(0, 0);
			rootNode.addInside(Enums.nodePosition.north, northNode);
			rootNode.addInside(Enums.nodePosition.west, westNode);

			var coords = westNode.coordinates();
			test.value(coords.x).isEqualTo(0);
			test.value(coords.y).isEqualTo(25);
		});

		it("east accounts for center and west", function()
		{
			var rootNode = new DockNode(100, 100);
			var westNode = new DockNode(0, 0);
			var eastNode = new DockNode(0, 0);
			rootNode.addInside(Enums.nodePosition.west, westNode);
			rootNode.addInside(Enums.nodePosition.east, eastNode);

			var coords = eastNode.coordinates();
			test.value(coords.x).isEqualTo(80);
			test.value(coords.y).isEqualTo(0);
		});

		it("south accounts for center and north", function()
		{
			var rootNode = new DockNode(100, 100);
			var northNode = new DockNode(0, 0);
			var southNode = new DockNode(0, 0);
			rootNode.addInside(Enums.nodePosition.north, northNode);
			rootNode.addInside(Enums.nodePosition.south, southNode);

			var coords = southNode.coordinates();
			test.value(coords.x).isEqualTo(0);
			test.value(coords.y).isEqualTo(80);
		});

		it("center is offset by sides", function()
		{
			var root = new DockNode();
			root.scaleTo(Enums.dimensions.width, 500);
			root.scaleTo(Enums.dimensions.height, 500);

			var westNode = new DockNode();
			root.addInside(Enums.nodePosition.west, westNode);

			test.value(root.center.coordinates().x).isEqualTo(25);
			test.value(westNode.center.coordinates().x).isEqualTo(0);
		});

		it("south nodes are spaced by the divider width", function()
		{
			var rootNode = new DockNode(100, 100);
			var southNode = new DockNode(0, 0);
			rootNode.addInside(Enums.nodePosition.south, southNode);
			test.value(southNode.center.coordinates().y).isEqualTo(80);
		});

		it("scenario1", function()
		{
			var root = new DockNode();
			root.addInside(Enums.nodePosition.west, new DockNode(0, 0));
			root.west.addInside(Enums.nodePosition.south, new DockNode(0, 0));
			root.west.south.addInside(Enums.nodePosition.south, new DockNode(0, 0));

			root.scaleTo(Enums.dimensions.width, 500);
			root.scaleTo(Enums.dimensions.height, 500);

			root.resize(Enums.nodePosition.west, 300);
			root.west.resize(Enums.nodePosition.south, 250);
			root.west.south.resize(Enums.nodePosition.south, 100);

			test.value(root.west.totalDimensionSize(Enums.dimensions.width)).isEqualTo(300);
			test.value(root.west.totalDimensionSize(Enums.dimensions.height)).isEqualTo(500);
			test.value(root.west.south.totalDimensionSize(Enums.dimensions.height)).isEqualTo(250);
			test.value(root.west.south.south.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
		});

		it("scenario2", function()
		{
			var root = new DockNode();
			root.addInside(Enums.nodePosition.west, new DockNode());
			root.west.addInside(Enums.nodePosition.south, new DockNode());
			root.west.south.addInside(Enums.nodePosition.south, new DockNode());
			root.defaultLayout(500, 500);

			test.value(root.west.totalDimensionSize(Enums.dimensions.width)).isEqualTo(95);
			test.value(root.west.south.totalDimensionSize(Enums.dimensions.width)).isEqualTo(95);
			test.value(root.west.south.south.totalDimensionSize(Enums.dimensions.width)).isEqualTo(95);

			test.value(root.west.totalDimensionSize(Enums.dimensions.height)).isEqualTo(500);
			test.value(root.west.south.totalDimensionSize(Enums.dimensions.height)).isEqualTo(95);
			test.value(root.west.south.south.totalDimensionSize(Enums.dimensions.height)).isEqualTo(14);
		});
	});

	describe("resolve coordinates to node", function()
	{
		it("scenario - west (south (south))", function()
		{
			var root = new DockNode(0, 0);
			root.addInside(Enums.nodePosition.west, new DockNode(0, 0));
			root.west.addInside(Enums.nodePosition.south, new DockNode(0, 0));
			root.west.south.addInside(Enums.nodePosition.south, new DockNode(0, 0));
			root.defaultLayout(500, 500);

			root.scaleTo(Enums.dimensions.height, 500);

			root.resize(Enums.nodePosition.west, 300);
			root.west.resize(Enums.nodePosition.south, 250);
			root.west.south.resize(Enums.nodePosition.south, 100);

			test.value(root.resolveCoordinatesToNode(305, 0).node).isEqualTo(root);
			test.value(root.resolveCoordinatesToNode(0, 0).node).isEqualTo(root.west);
			test.value(root.resolveCoordinatesToNode(1, 250).node).isEqualTo(root.west.south);
			test.value(root.resolveCoordinatesToNode(1, 400).node).isEqualTo(root.west.south.south);
			test.value(root.resolveCoordinatesToNode(1, 500).node).isEqualTo(root.west.south.south);

			test.value(root.resolveCoordinatesToNode(301, 0).container).isEqualTo(root);
		});
	});

	describe("scenarios", function()
	{
		it("scenario - north west", function()
		{
			var root = new DockNode(0, 0);
			root.addInside(Enums.nodePosition.north, new DockNode(0, 0));
			root.addInside(Enums.nodePosition.west, new DockNode(0, 0));
			root.defaultLayout(500, 500);

			root.resize(Enums.nodePosition.north, 100);
			root.resize(Enums.nodePosition.west, 100);

			test.value(root.childNodeDimensionSize(Enums.nodePosition.north, Enums.dimensions.width)).isEqualTo(500);
			test.value(root.childNodeDimensionSize(Enums.nodePosition.west, Enums.dimensions.width)).isEqualTo(100);
			test.value(root.childNodeDimensionSize(Enums.nodePosition.west, Enums.dimensions.height)).isEqualTo(395);

			root.scaleTo(Enums.dimensions.width, 1000);

			test.value(root.childNodeDimensionSize(Enums.nodePosition.north, Enums.dimensions.width)).isEqualTo(1000);

			root.scaleTo(Enums.dimensions.height, 1000);

			test.value(root.childNodeDimensionSize(Enums.nodePosition.west, Enums.dimensions.height)).isEqualTo(795);
			test.value(root.childNodeDimensionSize(Enums.nodePosition.north, Enums.dimensions.height)).isEqualTo(200);

		});
	});

	describe("defaultLayout", function()
	{
		it("sides added first are expanded first", function()
		{
			var root = new DockNode(0, 0);
			root.addInside(Enums.nodePosition.west, new DockNode(0, 0));
			root.addInside(Enums.nodePosition.north, new DockNode(0, 0));
			root.addInside(Enums.nodePosition.south, new DockNode(0, 0));

			root.defaultLayout(500, 500);

			test.value(root.childNodeDimensionSize(Enums.nodePosition.west, Enums.dimensions.width)).isEqualTo(95);
			test.value(root.childNodeDimensionSize(Enums.nodePosition.south, Enums.dimensions.width)).isEqualTo(400);
			test.value(root.childNodeDimensionSize(Enums.nodePosition.north, Enums.dimensions.width)).isEqualTo(400);
		});
	});

	describe("removeChild", function()
	{
		it("adjacent sides added after the node being removed expand to fill empty space", function()
		{
			var root = new DockNode(0, 0);
			root.addInside(Enums.nodePosition.west, new DockNode(0, 0));
			root.addInside(Enums.nodePosition.north, new DockNode(0, 0));
			root.addInside(Enums.nodePosition.south, new DockNode(0, 0));

			root.defaultLayout(500, 500);
			root.removeChild(Enums.nodePosition.west);

			test.value(root.childNodeDimensionSize(Enums.nodePosition.south, Enums.dimensions.width)).isEqualTo(500);
			test.value(root.childNodeDimensionSize(Enums.nodePosition.north, Enums.dimensions.width)).isEqualTo(500);
		});

		it("adjacent sides added before the removed node do not change size", function()
		{
			var root = new DockNode(0, 0);
			root.addInside(Enums.nodePosition.north, new DockNode(0, 0));
			root.addInside(Enums.nodePosition.west, new DockNode(0, 0));
			root.addInside(Enums.nodePosition.south, new DockNode(0, 0));

			root.defaultLayout(500, 500);
			root.removeChild(Enums.nodePosition.west);

			test.value(root.childNodeDimensionSize(Enums.nodePosition.south, Enums.dimensions.width)).isEqualTo(500);
			test.value(root.childNodeDimensionSize(Enums.nodePosition.north, Enums.dimensions.width)).isEqualTo(500);
		});
	});
});

