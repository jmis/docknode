var test = require("unit.js");
var DockingArea = require("../src/DockingArea.js");
var DockNode = require("../src/DockNode.js");
var Enums = require("../src/DockingEnums.js");

describe("Given a DockingArea", function()
{
	describe("when adding an node to the outside", function()
	{
		it("an already used side results in a new root", function()
		{
			var dockingArea = new DockingArea();
			dockingArea.root.scaleTo(Enums.dimensions.width, 100);
			dockingArea.root.scaleTo(Enums.dimensions.height, 100);

			var northNode = new DockNode();
			dockingArea.addOutside(Enums.nodePosition.north, northNode);

			test.value(dockingArea.root.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
			test.value(dockingArea.root.childNodeDimensionSize(Enums.nodePosition.center, Enums.dimensions.height)).isEqualTo(75);
			test.value(northNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(20);

			var oldRoot = dockingArea.root;
			var secondNorthNode = new DockNode();
			var newRoot = dockingArea.addOutside(Enums.nodePosition.north, secondNorthNode);

			test.value(dockingArea.root.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
			test.value(northNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(15);
			test.value(secondNorthNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(20);
			test.value(oldRoot.totalDimensionSize(Enums.dimensions.height)).isEqualTo(75);
		});

		it("a free side uses the existing root", function()
		{
			var dockingArea = new DockingArea();
			dockingArea.root.scaleTo(Enums.dimensions.width, 100);
			dockingArea.root.scaleTo(Enums.dimensions.height, 100);

			var northNode = new DockNode();
			dockingArea.addOutside(Enums.nodePosition.north, northNode);

			var southNode = new DockNode();
			dockingArea.addOutside(Enums.nodePosition.south, southNode);

			test.value(dockingArea.root.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
			test.value(northNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(20);
			test.value(southNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(20);
			test.value(dockingArea.root.childNodeDimensionSize(Enums.nodePosition.center, Enums.dimensions.height)).isEqualTo(50);

			dockingArea.root.resize(Enums.nodePosition.south, 40);

			test.value(dockingArea.root.childNodeDimensionSize(Enums.nodePosition.center, Enums.dimensions.height)).isEqualTo(30);
			test.value(dockingArea.root.totalDimensionSize(Enums.dimensions.height)).isEqualTo(100);
			test.value(northNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(20);
			test.value(southNode.totalDimensionSize(Enums.dimensions.height)).isEqualTo(40);
		});
	})
})
