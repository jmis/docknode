function BoundingBox(left, right, top, bottom)
{
	this.left = left;
	this.right = right;
	this.top = top;
	this.bottom = bottom;
};

BoundingBox.locationIsBetween = function (b1, b2, x, y)
{
	return (x > b1.right && x < b2.left && y >= b2.top && y <= b2.bottom)
		|| (x > b2.right && x < b1.left && y >= b2.top && y <= b2.bottom)
		|| (x >= b2.left && x <= b2.right && y > b1.bottom && y < b2.top)
		|| (x >= b2.left && x <= b2.right && y > b2.bottom && y < b1.top);
};

BoundingBox.locationIsInside = function(b, x, y)
{
	return x >= b.left && x <= b.right && y >= b.top && y <= b.bottom;
};

module.exports = BoundingBox;