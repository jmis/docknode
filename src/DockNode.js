var Enums = require("./DockingEnums.js");
var deepcopy = require("deep-copy");
var check = require("check-types");
var BoundingBox = require("./BoundingBox.js");

function DockNode(width, height)
{
	var self = this;
	this.north = null;
	this.south = null;
	this.east = null;
	this.west = null;
	this.sidesOrderedByTimeAdded = [];
	this.parent = null;

	this.center =
	{
		width: width || 0,
		height: height || 0,
		data: {},
		scaleTo: function (dimension, size) { this[dimension] = size; },
		totalDimensionSize: function(dimension) { return this[dimension]; },
		resize: function (dimension, size) { this[dimension] = size; },
		defaultLayoutForDimension: function(dimension, size) { this[dimension] = size; },
		setData: function (d) { this.data = d; },
		resolveCoordinatesToNode: function (x, y) { return { nodeType: Enums.nodeType.node, node: self }},
		coordinates: function()
		{
			var coords = self.coordinates();
			coords.x += self.childNodeDimensionSize(Enums.nodePosition.west, Enums.dimensions.width);
			coords.y += self.childNodeDimensionSize(Enums.nodePosition.north, Enums.dimensions.height);

			if (self.west) coords.x += Enums.numDividerPixels;
			if (self.north) coords.y += Enums.numDividerPixels;
			return coords;
		}
	};
};

DockNode.prototype.setData = function(d)
{
	this.center.setData(d);
	return this;
};

DockNode.prototype.wasAddedBefore = function(side1, side2)
{
	side1 = check.string(side1) ? this[side1] : side1;
	side2 = check.string(side2) ? this[side2] : side2;

	if (!side1 || !side2) return false;

	return this.sidesOrderedByTimeAdded.indexOf(side1) < this.sidesOrderedByTimeAdded.indexOf(side2);
};

DockNode.prototype.addInside = function (side, node)
{
	var adjustableDimension = Enums.adjustableDimensions[side];
	var fixedDimension = Enums.fixedDimensions[side];

	var fixedDimensionTotalSize = this.totalDimensionSize(fixedDimension);
	var adjacentSide1 = Enums.adjacentSides[side][0];
	var adjacentSide2 = Enums.adjacentSides[side][1];

	if (this[adjacentSide1]) fixedDimensionTotalSize = fixedDimensionTotalSize - this[adjacentSide1].totalDimensionSize(fixedDimension) - Enums.numDividerPixels;
	if (this[adjacentSide2]) fixedDimensionTotalSize = fixedDimensionTotalSize - this[adjacentSide2].totalDimensionSize(fixedDimension) - Enums.numDividerPixels;

	node.scaleTo(adjustableDimension, 20);
	node.scaleTo(fixedDimension, fixedDimensionTotalSize);

	this.center.scaleTo(adjustableDimension, this.center.totalDimensionSize(adjustableDimension) - node.totalDimensionSize(adjustableDimension) - Enums.numDividerPixels);
	this.sidesOrderedByTimeAdded.push(node);
	this[side] = node;
	node.parent = this;
};

DockNode.prototype.resize = function(side, newSideSize)
{
	if (side == Enums.nodePosition.center) throw new Error("Cannot resize center node.");
	if (!this[side]) throw new Error("Side doesn't exist for resizing: " + side);

	var dimension = Enums.adjustableDimensions[side];
	var oldSideSize = this.childNodeDimensionSize(side, dimension);
	var difference = newSideSize - oldSideSize;

	this.center.scaleTo(dimension, this.center.totalDimensionSize(dimension) + -difference);
	this[side].scaleTo(dimension, newSideSize);

	var side1 = Enums.adjacentSides[side][0];
	var side2 = Enums.adjacentSides[side][1];

	if (this.wasAddedBefore(side, side1)) this[side1].scaleTo(dimension, this[side1].totalDimensionSize(dimension) + -difference);
	if (this.wasAddedBefore(side, side2)) this[side2].scaleTo(dimension, this[side2].totalDimensionSize(dimension) + -difference);
};

DockNode.prototype.scaleTo = function (dimension, newTotalSize)
{
	var oldTotalSize = this.totalDimensionSize(dimension) - this.dividerSpaceForDimension(dimension) || 0;

	if (!oldTotalSize)
	{
		this.defaultLayoutForDimension(dimension, newTotalSize);
		return;
	}

	var newTotalSizeWithoutDividers = newTotalSize - this.dividerSpaceForDimension(dimension);
	var scaleFactor = newTotalSizeWithoutDividers / oldTotalSize;

	for (var position in Enums.nodePosition)
	{
		if (!this[position] || position == Enums.nodePosition.center) continue;

		if (Enums.fixedDimensions[position] == dimension)
		{
			var fixedDimensionTotalSize = newTotalSize;
			var adjacentSide1 = Enums.adjacentSides[position][0];
			var adjacentSide2 = Enums.adjacentSides[position][1];

			if (this.wasAddedBefore(adjacentSide1, position)) fixedDimensionTotalSize = fixedDimensionTotalSize - this[adjacentSide1].totalDimensionSize(dimension) - Enums.numDividerPixels;
			if (this.wasAddedBefore(adjacentSide2, position)) fixedDimensionTotalSize = fixedDimensionTotalSize - this[adjacentSide2].totalDimensionSize(dimension) - Enums.numDividerPixels;

			this[position].scaleTo(dimension, fixedDimensionTotalSize)
		}
		else
		{
			var defaultSideSize = Math.round(newTotalSizeWithoutDividers * .2);
			var updatedSideSize = Math.round(this.childNodeDimensionSize(position, dimension) * scaleFactor);
			var targetSideSize = oldTotalSize <= 0 || !isFinite(scaleFactor) ? defaultSideSize : updatedSideSize;
			this[position].scaleTo(dimension, targetSideSize);
		}
	}

	var side1 = Enums.adjustableDimensionToSide[dimension][0];
	var side2 = Enums.adjustableDimensionToSide[dimension][1];
	var centerSize = newTotalSizeWithoutDividers;
	centerSize -= this.childNodeDimensionSize(side1, dimension);
	centerSize -= this.childNodeDimensionSize(side2, dimension);
	this[Enums.nodePosition.center].scaleTo(dimension, centerSize);
};

DockNode.prototype.defaultLayout = function (width, height)
{
	this.defaultLayoutForDimension(Enums.dimensions.width, width);
	this.defaultLayoutForDimension(Enums.dimensions.height, height);
};

DockNode.prototype.defaultLayoutForDimension = function(dimension, newTotalSize)
{
	var positionsOrderedByTimeAdded = [];

	for (var key in this.sidesOrderedByTimeAdded)
	{
		for (var position in Enums.nodePosition)
		{
			if (this[position] == this.sidesOrderedByTimeAdded[key])
			{
				positionsOrderedByTimeAdded.push(position);
			}
		}
	}

	for (var key in positionsOrderedByTimeAdded)
	{
		var position = positionsOrderedByTimeAdded[key];

		if (!this[position] || position == Enums.nodePosition.center) continue;

		var isAdjustableAlongDimension = Enums.adjustableDimensionToSide[dimension].indexOf(position) != -1;

		if (isAdjustableAlongDimension)
		{
			this[position].defaultLayoutForDimension(dimension, Math.round(newTotalSize * .2) - Enums.numDividerPixels);
		}
		else
		{
			var fixedDimensionTotalSize = newTotalSize;
			var adjacentSide1 = Enums.adjacentSides[position][0];
			var adjacentSide2 = Enums.adjacentSides[position][1];

			if (this.wasAddedBefore(adjacentSide1, this[position])) fixedDimensionTotalSize = fixedDimensionTotalSize - this[adjacentSide1].totalDimensionSize(dimension) - Enums.numDividerPixels;
			if (this.wasAddedBefore(adjacentSide2, this[position])) if (this[adjacentSide2]) fixedDimensionTotalSize = fixedDimensionTotalSize - this[adjacentSide2].totalDimensionSize(dimension) - Enums.numDividerPixels;

			this[position].defaultLayoutForDimension(dimension, fixedDimensionTotalSize);
		}
	}

	var side1 = Enums.adjustableDimensionToSide[dimension][0];
	var side2 = Enums.adjustableDimensionToSide[dimension][1];
	var centerSize = newTotalSize;
	centerSize -= this.childNodeDimensionSize(side1, dimension);
	centerSize -= this.childNodeDimensionSize(side2, dimension);
	centerSize -= this.dividerSpaceForDimension(dimension);
	this[Enums.nodePosition.center].defaultLayoutForDimension(dimension, centerSize);
};

DockNode.prototype.dividerSpaceForDimension = function(dimension)
{
	var sides = Enums.adjustableDimensionToSide[dimension];
	var dividerSpace = 0;
	dividerSpace += this[sides[0]] ? 1 : 0;
	dividerSpace += this[sides[1]] ? 1 : 0;
	dividerSpace *= Enums.numDividerPixels;
	return dividerSpace;
};

DockNode.prototype.childNodeDimensionSize = function (position, dimension)
{
	return this[position] ? this[position].totalDimensionSize(dimension) : 0;
};

DockNode.prototype.totalDimensionSize = function (dimension)
{
	var sideOneName = Enums.adjustableDimensionToSide[dimension][0];
	var sideTwoName = Enums.adjustableDimensionToSide[dimension][1];
	var dividerSize = this.dividerSpaceForDimension(dimension);
	var sideOneDimension = this.childNodeDimensionSize(sideOneName, dimension);
	var centerDimension = this.childNodeDimensionSize(Enums.nodePosition.center, dimension);
	var sideTwoDimension = this.childNodeDimensionSize(sideTwoName, dimension);
	return centerDimension + sideOneDimension + sideTwoDimension + dividerSize;
};

DockNode.prototype.coordinates = function()
{
	if (!this.parent) return {x:0, y:0};

	var parentCoordinates = this.parent.coordinates();
	var myCoordinates = deepcopy(parentCoordinates);

	for (var sideName in Enums.nodePosition)
	{
		if (this.parent[sideName] == this)
		{
			var coordinateDefinition = Enums.coordinateDefinitions[sideName];

			for (var axis in coordinateDefinition)
			{
				var sideToAccountFor = coordinateDefinition[axis];

				if (!sideToAccountFor) continue;

				if (this.parent.wasAddedBefore(this.parent[sideToAccountFor], this))
				{
					var dimensionToConsider = Enums.coordinatesToDimensions[axis];

					myCoordinates[axis] = this.parent[sideToAccountFor].coordinates()[axis]
						+ this.parent[sideToAccountFor].totalDimensionSize(dimensionToConsider)
						+ Enums.numDividerPixels;
				}
			}
		}
	}

	return myCoordinates;
};

DockNode.prototype.resolveCoordinatesToNode = function(x, y)
{
	var centerBox = createBoundingBox(this.center);

	for (var sideName in Enums.nodePosition)
	{
		if (!this[sideName]) continue;

		var sideBox = createBoundingBox(this[sideName]);

		if (BoundingBox.locationIsInside(sideBox, x, y))
		{
			return this[sideName].resolveCoordinatesToNode(x, y);
		}

		if (BoundingBox.locationIsBetween(sideBox, centerBox, x, y))
		{
			return { nodeType: Enums.nodeType.divider, direction: Enums.adjustableDimensions[sideName], container: this, side: sideName };
		}
	}

	throw new Error("Couldn't resolve coordinates to node for coordinates: " + JSON.stringify({x:x, y:y}));
};

DockNode.prototype.removeChild = function (side)
{
	var sideToRemove = this[side];
	var adjacentSide1 = Enums.adjacentSides[side][0];
	var adjacentSide2 = Enums.adjacentSides[side][1];

	if (this[adjacentSide1] || this[adjacentSide2])
	{
		var dimensionToAdjust = Enums.fixedDimensions[adjacentSide1];
		var amountToAdjust = sideToRemove.totalDimensionSize(dimensionToAdjust) + Enums.numDividerPixels;

		if (this[adjacentSide1] && !this.wasAddedBefore(adjacentSide1, side))
		{
			this[adjacentSide1].scaleTo(dimensionToAdjust, this[adjacentSide1].totalDimensionSize(dimensionToAdjust) + amountToAdjust);
		}

		if (this[adjacentSide2] && !this.wasAddedBefore(adjacentSide2, side))
		{
			this[adjacentSide2].scaleTo(dimensionToAdjust, this[adjacentSide2].totalDimensionSize(dimensionToAdjust) + amountToAdjust);
		}
	}

	this[side] = null;
	this.sidesOrderedByTimeAdded.splice(this.sidesOrderedByTimeAdded.indexOf(this[side]), 1);
};

function createBoundingBox(node)
{
	var coords = node.coordinates();
	var width = node.totalDimensionSize(Enums.dimensions.width);
	var height = node.totalDimensionSize(Enums.dimensions.height);
	var left = coords.x;
	var right = left + width;
	var top = coords.y;
	var bottom = coords.y + height;
	return new BoundingBox(left, right, top, bottom);
};

module.exports = DockNode;
