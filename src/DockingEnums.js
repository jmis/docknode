exports.nodePosition =
{
	"north": "north",
	"south": "south",
	"east": "east",
	"west": "west",
	"center": "center"
};

exports.dimensions =
{
	"width": "width",
	"height": "height"
};

exports.adjacentSides =
{
	"north": ["east", "west"],
	"south": ["east", "west"],
	"east": ["north", "south"],
	"west": ["north", "south"]
};

exports.oppositeSides =
{
	"north": "south",
	"south": "north",
	"east": "west",
	"west": "east"
};

exports.adjustableDimensions =
{
	"north": "height",
	"south": "height",
	"east": "width",
	"west": "width"
};

exports.adjustableDimensionToSide =
{
	"height": ["north", "south"],
	"width": ["east", "west"]
};

exports.fixedDimensionToSide =
{
	"height": ["east", "west"],
	"width": ["north", "south"]
};

exports.fixedDimensions =
{
	"north": "width",
	"south": "width",
	"east": "height",
	"west": "height"
};

exports.coordinateDefinitions =
{
	"west": { x: "", y: "north"},
	"east": { x: "center", y: "north"},
	"north": { x: "west", y: ""},
	"south": { x: "west", y: "center"}
};

exports.coordinatesToDimensions =
{
	"x": "width",
	"y": "height"
};

exports.dimensionsToCoordinates =
{
	"width": "x",
	"height": "y"
};

exports.nodeType =
{
	"node": "node",
	"divider": "divider"
};

exports.numDividerPixels = 5;