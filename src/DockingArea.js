var DockNode = require("./DockNode.js");
var Enums = require("./DockingEnums.js");

function DockingArea()
{
	this.root = new DockNode();
};

DockingArea.prototype.addOutside = function (side, node)
{
	if (!this.root[side])
	{
		this.root.addInside(side, node);
	}
	else
	{
		var newRoot = new DockNode();
		newRoot.center = this.root;
		this.root.parent = newRoot;
		newRoot.addInside(side, node);
		this.root = newRoot;
	}
};

DockingArea.prototype.getContentNodes = function()
{
	var contentNodes = [];
	var queue = [this.root];

	while (queue.length)
	{
		var current = queue.shift();

		if (current.width) contentNodes.push(
		{
			x: current.coordinates().x,
			y: current.coordinates().y,
			width: current.width,
			height: current.height,
			data: current.data
		});

		if (current.west) queue.push(current.west);
		if (current.east) queue.push(current.east);
		if (current.north) queue.push(current.north);
		if (current.south) queue.push(current.south);
		if (current.center) queue.push(current.center);
	}

	return contentNodes;
};

module.exports = DockingArea;
